import numpy as np
from sklearn.datasets import make_blobs
from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import DBSCAN
from matplotlib import pyplot as plt

def cluster(data, min_samples=5):

    neigh = NearestNeighbors(n_neighbors=min_samples)
    nbrs = neigh.fit(data)
    distances, indices = nbrs.kneighbors(data)
    distances = distances[:,min_samples-1]
    distances = np.sort(distances, axis=0)

    # plt.plot(distances)
    # plt.show()
    clusters = []
    for epsilon in np.arange(0.001, 1, 0.002):
        db_scan = DBSCAN(eps=epsilon, min_samples=min_samples, metric="cosine")
        db_scan = db_scan.fit(data)
        clusters.append(len(set(db_scan.labels_)))
    plt.plot(np.arange(0.001, 1, 0.002), clusters)
    plt.show()

    db_scan = DBSCAN(eps=0.193, min_samples=min_samples, metric="cosine")
    db_scan = db_scan.fit(data)
    return db_scan


