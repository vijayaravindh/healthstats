import geopandas as gpd
import requests
from utils import FindTable, ExtractRowData, CreateGeopandasChoropleth, CreateFoliumChoropleth
from difflib import get_close_matches

def GetIndianStateData(api_url):
    headers = {
        "Accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Encoding" : "gzip, deflate, br",
        "Accept-Language" : "en-US,en;q=0.5",
        "Connection" : "keep-alive",
        "DNT" : "1",
        "Upgrade-Insecure-Requests"	: "1",
        "User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:81.0) Gecko/20100101 Firefox/81.0",
        }
    resp = requests.get(api_url, headers=headers)
    if resp.status_code == requests.codes.ok:
        return resp.json()
    else:
        resp.raise_for_status()

def GetIndianStats():
    state_data = GetIndianStateData("https://www.mohfw.gov.in/data/datanew.json")
    # Create a geopandas datframe from the shapefile and attach the stats to the dataframe
    india_shape_file_path = "india_admin_divisions/India_Administrative_Divisions.shp"
    india_map_with_stats = gpd.read_file(india_shape_file_path)[["st_nm", "geometry"]]
    india_map_with_stats["geometry"] = india_map_with_stats.simplify(tolerance=0.01)
    india_map_with_stats["total_cases"] = 0
    india_map_with_stats["recovered"] = 0
    india_map_with_stats["deaths"] = 0
    india_map_with_stats["active_cases"] = 0
    for state in state_data:
        if state['state_name']:
            closest_match_in_map = get_close_matches(state['state_name'], india_map_with_stats['st_nm'].tolist(), n=1)[0]
            india_map_with_stats.loc[india_map_with_stats.st_nm == closest_match_in_map, 
                                                                    [
                                                                    "active_cases", 
                                                                    "recovered", 
                                                                    "deaths",
                                                                    "total_cases"]] = [ int(state["new_active"]),
                                                                                        int(state["new_cured"]),
                                                                                        int(state["new_death"]),
                                                                                        int(state["new_positive"]),
                                                                                    ]
    return india_map_with_stats


def RenderChoroplethPyplot():
    indian_map_with_stats = GetIndianStats()
    plot_data = {
        "cmap":'OrRd', 
        "linewidth":0.8, 
        "edgecolor":'0.8', 
        "legend":True,
    }
    axis_data = {
        "title" : {
            "label":"COVID-19 Positive Cases", 
            "fontdict":{'fontsize': '20', 'fontweight' : '1'},
        },
        "annotate" : {
            "s":"Source: Ministry of Health and Family Welfare, Government of India",
            "xy":(0.2, 0.05), 
            "xycoords":"figure fraction",
            "horizontalalignment":"left", 
            "verticalalignment":"top",
            "fontsize":10, 
            "color":"#555555",
        }
    }
    CreateGeopandasChoropleth(indian_map_with_stats, "total_cases", plot_data, axis_data)

def RenderChoroplethFolium(stat_type):
    indian_map_with_stats = GetIndianStats()
    map_layer_attributes = {
        "map" : {
            "location" : [23, 77],
            "tiles" : "Mapbox Bright",
            "zoom_start": 4.5,
            "min_zoom": 4.5,
            "max_zoom": 4.5,
            "noWrap": True,
            "zoomDelta": 0.5,
            "zoomControl" : False,
            "dragging" : False,
        },
        "bounds" : {
            "bounds" : [[7, 68] ,[37, 97]]
        },
    }
    choropleth_attributes = {
        "name" : "india-states",
        "key_on" : "feature.properties.st_nm",
        "fill_color" : "YlGn",
        "fill_opacity" : 0.7,
        "line_opacity" : 0.2,
        "highlight" : True,
    }
    return CreateFoliumChoropleth(indian_map_with_stats, ["st_nm", stat_type], map_layer_attributes, choropleth_attributes)
