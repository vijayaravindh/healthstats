# COVID-19 Dashboard India
Real-time dashboard showing the Active and Total cases and other stats in each state in India based on data from Health Ministry of India (https://www.mohfw.gov.in/)

LINK: https://health-stats-india.herokuapp.com/

## Covid-19 Dashboard
![image](covid-india.gif)

### Installation and execution
```
python3 -m venv env
source bin/env/activate
pip install -r requirements.txt
flask run
```
