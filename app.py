from flask import Flask, request
from flask_minify import minify, decorators
from india_covid19 import RenderChoroplethFolium

app = Flask(__name__)
minify(app=app, passive=True)

@app.route('/covid-india')
@decorators.minify(html=True, js=True, cssless=True)
def covid_india():
    stat_type = request.args["type"]
    folium_map = RenderChoroplethFolium(stat_type)
    return folium_map._repr_html_()

@app.route('/')
@decorators.minify(html=True, js=True, cssless=True)
def index():
    return """
    <!DOCTYPE html>
    <html style="height:100%">
    <head>
        <title>COVID-19 India Live</title>
        <style>
            .flex-container {
            display: flex;
            flex-direction: column;
            justify-content: center;
            height:100%
            }
            .flex-container > h1 {
            margin: 10px;
            padding: 20px;
            font-size: 30px;
            align-self: center;
            }
            .flex-container > iframe {
            margin: 10px;
            padding: 20px;
            font-size: 30px;
            height : 80%;
            }
            .flex-container > select {
            margin: 10px;
            padding: 20px;
            font-size: 15px;
            width: 30%;
            align-self: center;
            }
        </style>
    </head>
    <body style="margin: auto; height:100%"> 
        <div class="flex-container">
            <h1>COVID-19 India</h1>
            <select name="type" id="type" onchange="changeMapType(this)">
                <option value="active_cases">Active Cases</option>
                <option value="total_cases">Total_Cases</option>
            </select>
            <iframe src="https://health-stats-india.herokuapp.com/covid-india?type=active_cases" name="map" id="map"></iframe>
        </div> 
        <script>
            function changeMapType(selectedItem) {
                document.getElementById('map').src = "https://health-stats-india.herokuapp.com/covid-india?type=" + selectedItem.value;
            }
        </script> 
    </body>
    </html>
 """

if __name__ == '__main__':
    app.run(debug=True)