from bs4 import BeautifulSoup
import requests as req
import matplotlib.pyplot as plt
import folium
from folium.features import GeoJsonTooltip


def FindTable(url, attributes=None):
	resp = req.get(url)
	soup = BeautifulSoup(resp.text, 'lxml')
	attr = dict()
	if attributes is not None:
		attr = attributes
	tables = soup.find_all('table', attrs=attr)
	return tables

def ExtractRowData(table):
	table_body = table.find('tbody')
	rows_list = list()
	rows = table_body.find_all('tr')
	for row in rows:
	    row_cells = row.find_all('td')
	    rows_list.append([ele.text.strip() for ele in row_cells])
	return rows_list

def CreateFoliumChoropleth(geopandas_dataframe, columns, map_layer_attributes, choropleth_attributes):
    folium_map = folium.Map(**map_layer_attributes["map"])
    if "bounds" in  map_layer_attributes.keys():
    	folium_map.fit_bounds(**map_layer_attributes["bounds"])
    choropleth = folium.Choropleth(geo_data = geopandas_dataframe,
					  data=geopandas_dataframe,
					  columns=columns,
					  **choropleth_attributes,
					  smooth_factor=0.001).add_to(folium_map)
    folium.GeoJsonTooltip(fields=['st_nm', 'total_cases', 'active_cases', 'recovered', 'deaths'],
				   								   aliases=['State : ', 'Total Cases :', 'Active Cases : ','Recovered :', 'Deaths :'], 
												   labels=True, 
                                                   sticky=True,
                         ).add_to(choropleth.geojson)
    ###### To Remove Color Scale
	# for key in choropleth._children:
    # 	if key.startswith('color_map'):
    #     	del(choropleth._children[key])
    return folium_map

def CreateGeopandasChoropleth(geopandas_dataframe, variable, plot_data, axis_data):
    vmin, vmax = geopandas_dataframe[[variable]].min(axis=1), geopandas_dataframe[[variable]].max(axis=1)
    fig, ax = plt.subplots(1, figsize=(10, 6))
    geopandas_dataframe.plot(column=variable, ax=ax, **plot_data)
    if 'title' in axis_data.keys():
        ax.set_title(**axis_data['title'])
    if 'annotate' in axis_data.keys():
        ax.annotate(**axis_data['annotate'])
    plt.show()
