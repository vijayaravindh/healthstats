import json
import pandas as pd
import spacy
import numpy as np
from cluster_points import cluster

news_articles = json.load(open("example_news.json"))["articles"]

article_table = pd.DataFrame(columns=['title', 'description', 'date'])
for article in news_articles:
    article_table = article_table.append(pd.Series({'title' : article['title'],
                                                    'description' : article['description'],
                                                    'date' : article['publishedAt']}), ignore_index=True)
article_table.drop_duplicates('title', inplace=True)
article_table.dropna(inplace=True)

nlp = spacy.load('en_core_web_lg')

sent_vectors = {}
docs = []

for index, item in article_table.iterrows():
    title_vector = nlp(item["title"])
    docs.append(title_vector)
    sent_vectors[item["title"]] = title_vector.vector

sentences = list(sent_vectors.keys())
vectors = list(sent_vectors.values())

data = np.array(vectors)

db_scan =  cluster(data, 2)
results = pd.DataFrame({'label' : db_scan.labels_,
                        'sent' : sentences})
example = results[results.label == 0].sent.tolist()
for ex in example:
    print(ex)
